//
//  APIClient.swift
//  CarListing
//
//  Created by Stanley Darmawan on 18/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Alamofire

class APIClient {
	private let serviceBaseURL = "http://retailapi-v2.dev.carsales.com.au"
	
	func getListing(completionHandler: @escaping (_ response: Result<CarListResponse>) -> Void) {
		let path = "/stock/car/test/v1/listing"
		let params = ["username": "test",
									"password": "2h7H53eXsQupXvkz"]
		Alamofire.request(serviceBaseURL+path, method: .get, parameters: params).responseDecodable { (response: DataResponse<CarListResponse>) in
			completionHandler(response.result)
		}
	}
	
	func getDetails(path: String, completionHandler: @escaping (_ response: Result<CarDetailsResponse>) -> Void) {
		let params = ["username": "test",
									"password": "2h7H53eXsQupXvkz"]
		Alamofire.request(serviceBaseURL+path, method: .get, parameters: params).responseDecodable { (response: DataResponse<CarDetailsResponse>) in
			completionHandler(response.result)
		}
	}
}
