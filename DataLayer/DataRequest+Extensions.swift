//
//  DataRequest+Extensions.swift
//  CarListing
//
//  Created by Stanley Darmawan on 18/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Alamofire

extension DataRequest {
	
	private func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
		return DataResponseSerializer { _, response, data, error in
			guard error == nil else { return .failure(error!) }
			
			guard let data = data else {
				return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
			}
			
			return Result { try JSONDecoder().decode(T.self, from: data) }
		}
	}
	
	@discardableResult
	func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
		return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
	}
}
