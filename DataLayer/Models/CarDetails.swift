//
//  CarDetails.swift
//  CarListing
//
//  Created by Stanley Darmawan on 19/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Foundation

struct CarDetailsResponse: Decodable {
	let carDetailsList: [CarDetails]
	
	enum CodingKeys: String, CodingKey {
		case carDetailsList = "Result"
	}
}

struct CarDetails: Decodable {
	let id: String
	let saleStatus: String
	let overview: CarDetailsOverview
	let comments: String
	
	enum CodingKeys: String, CodingKey {
		case id = "Id"
		case saleStatus = "SaleStatus"
		case overview = "Overview"
		case comments = "Comments"
	}
}

struct CarDetailsOverview: Decodable {
	let location: String
	let price: String
	let photosURLs: [String]
	
	enum CodingKeys: String, CodingKey {
		case location = "Location"
		case price = "Price"
		case photosURLs = "Photos"
	}
}

