//
//  Car.swift
//  CarListing
//
//  Created by Stanley Darmawan on 18/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Foundation

struct CarListResponse: Decodable {
	let cars: [Car]
	
	enum CodingKeys: String, CodingKey {
		case cars = "Result"
	}
}

struct Car: Decodable {
	let id: String
	let title: String
	let location: String
	let price: String
	let mainPhotoURL: String
	let detailsURL: String
	
	enum CodingKeys: String, CodingKey {
		case id = "Id"
		case title = "Title"
		case location = "Location"
		case price = "Price"
		case mainPhotoURL = "MainPhoto"
		case detailsURL = "DetailsUrl"
	}
}
