//
//  DetailsViewController.swift
//  CarListing
//
//  Created by Stanley Darmawan on 19/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import UIKit
import SDWebImage

class DetailsViewController: UIViewController {
	var carDetails: CarDetails?
	
	@IBOutlet var locationLabel: UILabel!
	@IBOutlet var priceLabel: UILabel!
	@IBOutlet var saleStatusLabel: UILabel!
	@IBOutlet var commentsLabel: UILabel!
	@IBOutlet var imageScrollView: UIScrollView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		imageScrollView.delegate = self
		
		guard let carDetails = carDetails else { return }

		locationLabel.text = carDetails.overview.location
		priceLabel.text = carDetails.overview.price
		saleStatusLabel.text = carDetails.saleStatus
		commentsLabel.text = carDetails.comments
		
		createImageSlides(from: carDetails.overview.photosURLs)
	}

	private func createImageSlides(from photoURLs: [String]) {
		imageScrollView.contentSize = CGSize(width: view.frame.width * CGFloat(photoURLs.count), height: imageScrollView.frame.height)
		imageScrollView.isPagingEnabled = true
		imageScrollView.showsVerticalScrollIndicator = false
		
		for i in 0 ..< photoURLs.count {
			let imageView = UIImageView(
				frame: CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.width * 2 / 3)
			)
			
			imageView.contentMode = .scaleAspectFill
			
			// load the first image
			if i == 0 {
				if let photoURLString = carDetails?.overview.photosURLs.first,
					let photoURL = URL(string: photoURLString) {
					imageView.sd_setImage(with: photoURL,
																placeholderImage: UIImage(named: "noImage"),
																completed: nil)
				}
			}
			imageScrollView.addSubview(imageView)
		}
	}
}

extension DetailsViewController: UIScrollViewDelegate {
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		let pageIndex = Int(round(scrollView.contentOffset.x/view.frame.width))
		
		if let photoURLString = carDetails?.overview.photosURLs[pageIndex],
			let photoURL = URL(string: photoURLString),
			let imageView = scrollView.subviews[pageIndex] as? UIImageView {			
			imageView.sd_setImage(with: photoURL,
														placeholderImage: UIImage(named: "noImage"),
														completed: nil)
		}
	}
}
