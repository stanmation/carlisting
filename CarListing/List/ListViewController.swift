//
//  ViewController.swift
//  CarListing
//
//  Created by Stanley Darmawan on 18/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import UIKit

final class ListViewController: UIViewController {
	@IBOutlet var progressIndicator: UIActivityIndicatorView!
	@IBOutlet var tableView: UITableView!
	
	let apiClient = APIClient()
	lazy var presenter = ListPresenter(display: self)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		presenter.getListing()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let vc = segue.destination as? DetailsViewController,
			let carDetails = sender as? CarDetails {
			vc.carDetails = carDetails
		}
	}
}

extension ListViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return presenter.cars.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let identifier = String(describing: ListCell.self)
		if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ListCell {
			let car = presenter.cars[indexPath.row]
			cell.configure(with: car)
			return cell
		}
		fatalError("Error in creating cells")
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		presenter.getDetails(atIndex: indexPath.row)
		tableView.deselectRow(at: indexPath, animated: true)
	}
}

extension ListViewController: ListDisplay {
	func showLoadingIndicator() {
		progressIndicator.isHidden = false
	}
	
	func hideLoadingIndicator() {
		progressIndicator.isHidden = true
	}
	
	func reload() {
		tableView.reloadData()
	}
	
	func showErrorAlert() {
		let alertController = UIAlertController(title: "Error", message: "An error has occured", preferredStyle: .alert)
		let action = UIAlertAction(title: "OK", style: .default, handler: nil)
		alertController.addAction(action)
		present(alertController, animated: true, completion: nil)
	}
	
	func navigateToDetails(of car: CarDetails) {
		performSegue(withIdentifier: "ToDetails", sender: car)
	}
}

