//
//  ListViewPresenter.swift
//  CarListing
//
//  Created by Stanley Darmawan on 18/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import Foundation
import Alamofire

protocol ListDisplay: class {
	func showLoadingIndicator()
	func hideLoadingIndicator()
	func reload()
	func showErrorAlert()
	func navigateToDetails(of car: CarDetails)
}

final class ListPresenter {
	var cars = [Car]()
	
	unowned let display: ListDisplay
	let apiClient: APIClient
	
	var isFetching = false
	
	init(display: ListDisplay,
			 apiClient: APIClient = APIClient()) {
		self.display = display
		self.apiClient = apiClient
	}
	
	func getListing() {
		if isFetching { return }
		
		isFetching = true
		display.showLoadingIndicator()
		
		apiClient.getListing { [weak self] result in
			self?.isFetching = false
			self?.display.hideLoadingIndicator()
			
			switch result {
			case .success(let carListResponse):
				self?.cars = carListResponse.cars
			case .failure(_):
				self?.display.showErrorAlert()
			}
			self?.display.reload()
		}
	}
	
	func getDetails(atIndex index: Int) {
		if isFetching { return }

		isFetching = true
		display.showLoadingIndicator()

		let selectedCar = cars[index]
		
		apiClient.getDetails(path: selectedCar.detailsURL) { [weak self] result in
			self?.isFetching = false
			self?.display.hideLoadingIndicator()
			
			switch result {
			case .success(let carDetailsResponse):
				if let carDetails = carDetailsResponse.carDetailsList.first {
					self?.display.navigateToDetails(of: carDetails)
				} else {
					self?.display.showErrorAlert()
				}
			case .failure(_):
				self?.display.showErrorAlert()
			}
			self?.display.reload()
		}
	}
}
