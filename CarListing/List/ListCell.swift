//
//  ListCell.swift
//  CarListing
//
//  Created by Stanley Darmawan on 18/2/19.
//  Copyright © 2019 Stanley Darmawan. All rights reserved.
//

import UIKit
import SDWebImage

class ListCell: UITableViewCell {
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var priceLabel: UILabel!
	@IBOutlet var locationLabel: UILabel!
	@IBOutlet var carImageView: UIImageView!
	
	func configure(with car: Car) {
		titleLabel.text = car.title
		priceLabel.text = car.price
		locationLabel.text = car.location
		carImageView.sd_setImage(with: URL(string: car.mainPhotoURL),
														 completed: nil)
	}
}
